import * as React from "react";
import "./Links.scss";
interface LinksProps {
    /** Click callback  */
    onClick?: React.MouseEventHandler<HTMLButtonElement>;
    LinksType?: "default" | "primary" | "success" | "warning" | "danger" | "pure" | "toggle";
    className?: string;
    href?: string;
}
export declare class Links extends React.Component<LinksProps> {
    static defaultProps: {
        LinksType: string;
        href: string;
    };
    render(): JSX.Element;
}
export {};

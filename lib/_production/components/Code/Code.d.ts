import * as React from "react";
import "./Code.scss";
interface CodeProps {
    children?: string;
    className?: string;
}
export declare class Code extends React.Component<CodeProps> {
    static defaultProps: {};
    render(): JSX.Element;
}
export {};

import * as React from "react";
import "./FlexComponent.scss";
interface FlexComponentProps {
    children?: any;
    width?: string;
    switchflow: boolean;
    className?: string;
    background?: "primary" | "secondary" | "third";
    lighten?: 0 | 20 | 40 | 60 | 80;
}
export declare class FlexComponent extends React.Component<FlexComponentProps> {
    static defaultProps: {
        width: string;
        switchflow: boolean;
        className: string;
    };
    render(): JSX.Element;
}
export {};

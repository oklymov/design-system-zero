import * as React from "react";
import "./DataEntry.scss";
interface DataEntryProps {
    /** Click callback  */
    InputType?: "default" | "success" | "warning" | "danger";
    /** Apply disabled styling and remove pointer events */
    label?: string;
    direction?: "ltr" | "rtl" | "default";
    disabled?: boolean;
    defaultChecked?: boolean;
    className?: string;
    placeholder?: string;
    value?: string;
    name?: string;
    id?: string;
    type?: "text" | "password" | "email" | "number" | "range" | "tel" | "url" | "search" | "reset" | "checkbox" | "radio" | "submit";
}
export declare class DataEntry extends React.Component<DataEntryProps> {
    static defaultProps: {
        InputType: string;
        type: string;
        disabled: boolean;
        checked: boolean;
        placeholder: string;
        direction: string;
    };
    render(): JSX.Element;
}
export {};

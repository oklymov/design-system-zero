import * as React from "react";
import "./Mini.scss";
interface MiniProps {
    Color?: "primary" | "secondary" | "hint";
    uppercase?: boolean;
    className?: string;
}
export declare class Mini extends React.Component<MiniProps> {
    static defaultProps: {
        Color: string;
    };
    render(): JSX.Element;
}
export {};

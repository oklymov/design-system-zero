import * as React from "react";
import "./heading.scss";
interface HeadingProps {
    Color?: "primary" | "secondary" | "hint";
    className?: string;
    size: string;
    children: any;
}
export declare class Heading extends React.Component<HeadingProps> {
    static defaultProps: {
        Color: string;
        size: string;
    };
    render(): JSX.Element;
}
export {};

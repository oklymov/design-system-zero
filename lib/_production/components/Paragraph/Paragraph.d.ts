import * as React from "react";
import "./paragraph.scss";
interface ParagraphProps {
    Color?: "primary" | "secondary" | "hint";
    Size?: "paragraph" | "subtitle" | "big_subtitle";
    className?: string;
}
export declare class Paragraph extends React.Component<ParagraphProps> {
    static defaultProps: {
        Color: string;
        Size: string;
    };
    render(): JSX.Element;
}
export {};

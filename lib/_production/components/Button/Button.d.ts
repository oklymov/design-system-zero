import * as React from "react";
import "./Button.scss";
interface ButtonProps {
    /** Click callback  */
    onClick?: React.MouseEventHandler<HTMLButtonElement>;
    buttonType?: "default" | "primary" | "success" | "warning" | "danger" | "toggle";
    /** Apply disabled styling and remove pointer events */
    disabled?: boolean;
    className?: string;
    type?: string;
}
export declare class Button extends React.Component<ButtonProps> {
    static defaultProps: {
        buttonType: string;
        disabled: boolean;
        type: string;
    };
    sendNot: () => void;
    render(): JSX.Element;
}
export {};

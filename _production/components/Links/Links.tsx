import * as React from "react";
import classNames from "classnames";
import "./Links.scss";

interface LinksProps {
  /** Click callback  */
  onClick?: React.MouseEventHandler<HTMLButtonElement>;

  LinksType?:
    | "default"
    | "primary"
    | "success"
    | "warning"
    | "danger"
    | "pure"
    | "toggle";

  className?: string;
  href?: string;
}
export class Links extends React.Component<LinksProps> {
  static defaultProps = {
    LinksType: "default",
    href: "#"
  };
  public render() {
    const { className, children, LinksType, href, ...other } = this.props;
    return (
      <a
        className={classNames(
          "ui_form_button",
          `ui_form_button--${LinksType}`,
          className
        )}
        href={href}
      >
        {children}
      </a>
    );
  }
}

// export default Links;

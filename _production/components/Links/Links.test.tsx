import * as React from 'react';
import { shallow } from 'enzyme';
import Links from './Links';

describe('<Links />', () => {
  test('renders', () => {
    const wrapper = shallow(<Links />);
    expect(wrapper).toMatchSnapshot();
  });
});
  
import * as React from "react";
import classNames from "classnames";

import "./Code.scss";
interface CodeProps {
  children?: string,
  className?: string;
}
export class Code extends React.Component<CodeProps> {
  static defaultProps = {

  };
  public render() {
    const {
      className,
        children,
      ...other
    } = this.props;
    return (
        <pre>
          <code className={classNames(
            className
          )}>
            {children}
          </code>
        </pre>
    );
  }
}

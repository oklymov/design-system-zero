import * as React from "react";
import classNames from "classnames";

import "./FlexComponent.scss";
interface FlexComponentProps {
  children?: any;
  width?: string;
  switchflow: boolean;
  className?: string;
  background?:
  | "primary"
  | "secondary"
  | "third";
  lighten?:
  | 0
  | 20
  | 40
  | 60
  | 80
}
export class FlexComponent extends React.Component<FlexComponentProps> {
  static defaultProps = {
    width: "100%",
    switchflow: false,
    className: "flex-container"
  };
  public render() {
    const {
      className,
      // style,
      width,
      background,
        lighten,
      switchflow,
      children,
      ...other
    } = this.props;
    const style = {
      width: width,
      minHeight: "100px"
    };
    return (
      <div
        style={style}
        className={classNames(
            `ui_base_bg--${background}`,
            `lighten--${lighten}`,
            className, { switchflow: switchflow })}
      >
        {children}
      </div>
    );
  }
}

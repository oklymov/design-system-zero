import * as React from "react";
import classNames from "classnames";

import "./heading.scss";
interface HeadingProps {
  Color?: "primary" | "secondary" | "hint";
  className?: string;
  size: string;
  children: any;
}
export class Heading extends React.Component<HeadingProps> {
  static defaultProps = {
    Color: "primary",
    size: "3"
  };
  public render() {
    const { className, children, size, Color, ...other } = this.props;
    const H = "h" + this.props.size;

    return (
      <H
        className={classNames(
          "title",
          `system_font_color--${Color}`,
          className
        )}
      >
        {children}
      </H>
    );
  }
}

import * as React from "react";
import classNames from "classnames";
import "./Button.scss";

interface ButtonProps {
  /** Click callback  */
  onClick?: React.MouseEventHandler<HTMLButtonElement>;

  buttonType?:
    | "default"
    | "primary"
    | "success"
    | "warning"
    | "danger"
    | "toggle";
  /** Apply disabled styling and remove pointer events */
  disabled?: boolean;
  /* Optional classes for eventual customization */
  className?: string;
  type?: string;
}
export class Button extends React.Component<ButtonProps> {
  static defaultProps = {
    buttonType: "default",
    disabled: false,
    type: "button"
  };

  // send notification to slack by pressing on the button ---->
  sendNot = () => {
    fetch(
      "https://hooks.slack.com/services/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",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          text:
            ":fire: :fire: :fire: It's a freaking magic :crystal_ball: :crystal_ball: :crystal_ball:",
          username: "DS Zero: Framer Prototype"
        })
      }
    )
      .then(function(response) {
        // alert(response.headers.post('Content-Type')); // application/json; charset=utf-8
        // alert(response.status); // 200
        // return response.json();
      })
      // .then(function(user) {
      //   alert(user.name); // iliakan
      // })
      .catch(alert);
  };
  // <--- end send notification to slack by pressing on the button

  public render() {
    const {
      className,
      children,
      buttonType,
      disabled,
      type,
      ...other
    } = this.props;
    return (
      <button
        className={classNames(
          "ui_form_button",
          `ui_form_button--${buttonType}`,
          className
        )}
        onClick={this.sendNot}
        type={type}
        disabled={disabled && true}
      >
        {children}
      </button>
    );
  }
}

// export default Button;

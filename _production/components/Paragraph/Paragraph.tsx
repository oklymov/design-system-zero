import * as React from "react";
import classNames from "classnames";

import "./paragraph.scss";
interface ParagraphProps {
  Color?:
    | "primary"
    | "secondary"
    | "hint";
  Size?:
  | "paragraph"
  | "subtitle"
  | "big_subtitle";
  className?: string;
}
export class Paragraph extends React.Component<ParagraphProps> {
  static defaultProps = {
    Color: "primary",
    Size: "paragraph"
  };
  public render() {
    const {
      className,
      children,
      Size,
      Color,
      ...other
    } = this.props;
    return (
      <p className={classNames(
        "title",
        `system_font_color--${Color}`,
        `font_size--${Size}`,
        className
      )}>
        {children}
      </p>
    );
  }
}

import * as React from "react";
import classNames from "classnames";

import "./Mini.scss";
interface MiniProps {
  Color?:
    | "primary"
    | "secondary"
    | "hint";
  uppercase?:boolean,
  className?: string;
}
export class Mini extends React.Component<MiniProps> {
  static defaultProps = {
    Color: "primary",
  };
  public render() {
    const {
      className,
      children,
        uppercase,
      Color,
      ...other
    } = this.props;
    return (
      <p className={classNames(
        "mini",
        {ttu: uppercase },
        `system_font_color--${Color}`,
        className
      )}>
        {children}
      </p>
    );
  }
}

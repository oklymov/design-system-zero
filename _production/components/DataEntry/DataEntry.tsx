import * as React from "react";
import classNames from "classnames";
import "./DataEntry.scss";

interface DataEntryProps {
  /** Click callback  */
  InputType?:
    | "default"
    | "success"
    | "warning"
    | "danger";
  /** Apply disabled styling and remove pointer events */
  label?:string,

  direction?:
  | "ltr"
  | "rtl"
  | "default";

  disabled?: boolean;
  defaultChecked?: boolean;
  /* Optional classes for eventual customization */
  className?: string;
  placeholder?: string;
  value?: string;
  name?: string;
  id?: string;
  type?:
  | "text"
  | "password"
  | "email"
  | "number"
  | "range"
  | "tel"
  | "url"
  | "search"
  | "reset"
  | "checkbox"
  | "radio"
  | "submit";
}
export class DataEntry extends React.Component<DataEntryProps> {
  static defaultProps = {
    InputType: "default",
    type: "text",
    disabled: false,
    checked: false,
    placeholder: "Placeholder",
    direction:"default"
  };
  public render() {
    const {
      InputType,
      type,
      value,
      direction,
      label,
      name,
      id,
      placeholder,
      className,
      disabled,
        defaultChecked,
      ...other
    } = this.props;
    const labelText = this.props.label?<label htmlFor={id}>{label}</label>:null
    return (
        <div
            className={classNames(
          "ui_form_input",
          `ui_form_input--${InputType}`,
          `ui_form_input--${type}`,
          `direction--${direction}`,
          className
          )}>

            <input
                name={name}
                id={id}
                type={type}
                value={value}
                placeholder={placeholder}
                disabled={disabled && true}
                defaultChecked={defaultChecked}
            />
          {labelText}
        </div>

    );
  }
}


import * as React from "react";
import { PropertyControls, ControlType } from "framer";
import { Paragraph as _Paragraph } from "../lib";

interface Props {
  text: string;
}

export class Paragraph extends React.Component<Props> {
  render() {
    return <_Paragraph {...this.props}>{this.props.text}</_Paragraph>;
  }

  static defaultProps: Props = {
    text: "Hello World!"
  };

  static propertyControls: PropertyControls<Props> = {
    text: { type: ControlType.String, title: "Text" }
  };
}

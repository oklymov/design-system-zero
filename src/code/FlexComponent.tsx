import * as React from "react";
import { FlexComponent as _FlexComponent } from "../lib";
import { PropertyControls, ControlType } from "framer";
// Define type of property
interface Props {
  text: string;
  switchflow: boolean;
  className?: string;
  background?: "primary" | "secondary" | "third";
  lighten?: "0" | "20" | "40" | "60" | "80";
}
export class FlexComponent extends React.Component<Props> {
  // Set default properties
  static defaultProps = {
    text: "Hello World!",
    switchflow: false
  };
  // Items shown in property panel
  static propertyControls: PropertyControls = {
    text: { type: ControlType.String, title: "Text" },
    background: {
      type: ControlType.Enum,
      title: "background",
      options: ["primary", "secondary", "third"]
    },
    lighten: {
      type: ControlType.Enum,
      title: "lighten",
      options: ["0", "20", "40", "60", "80"]
    },
    multikids: {
      type: ControlType.ComponentInstance,
      title: "Children"
    }
  };
  render() {
    return (
      <_FlexComponent {...this.props}>{this.props.children}</_FlexComponent>
    );
  }
}

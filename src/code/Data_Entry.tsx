import * as React from "react";
import { DataEntry as _DataEntry } from "../lib";
import { PropertyControls, ControlType } from "framer";
// Define type of property
interface Props {
  /** Click callback  */
  InputType?: "default" | "success" | "warning" | "danger";
  /** Apply disabled styling and remove pointer events */
  label?: string;
  direction?: "ltr" | "rtl" | "default";
  disabled?: boolean;
  defaultChecked?: boolean;
  /* Optional classes for eventual customization */
  className?: string;
  placeholder?: string;
  value?: string;
  name?: string;
  id?: string;
  type?:
    | "text"
    | "password"
    | "email"
    | "number"
    | "range"
    | "tel"
    | "url"
    | "search"
    | "reset"
    | "checkbox"
    | "radio"
    | "submit";
}
export class DataEntry extends React.Component<Props> {
  // Set default properties
  static defaultProps = {
    InputType: "default",
    type: "text",
    disabled: false,
    checked: false,
    placeholder: "Placeholder",
    direction: "default"
  };
  // Items shown in property panel
  static propertyControls: PropertyControls = {
    type: {
      type: ControlType.Enum,
      title: "Type",
      options: [
        "text",
        "password",
        "checkbox",
        "radio",
        "email",
        "number",
        "range",
        "tel",
        "url",
        "search",
        "reset",
        "submit"
      ]
    },
    InputType: {
      type: ControlType.Enum,
      title: "State",
      options: ["default", "success", "warning", "danger"]
    },
    placeholder: { type: ControlType.String, title: "Placeholder" },
    value: { type: ControlType.String, title: "Value" },
    label: { type: ControlType.String, title: "Label" },
    direction: {
      type: ControlType.Enum,
      title: "Label direction",
      options: ["default", "ltr", "rtl"]
    },
    id: { type: ControlType.String, title: "Identification" },
    name: { type: ControlType.String, title: "Inputs Group" },
    disabled: {
      type: ControlType.Boolean,
      title: "Disable element",
      disabledTitle: "Nope",
      enabledTitle: "Yep"
    },
    defaultChecked: {
      type: ControlType.Boolean,
      title: "Is Checked?",
      disabledTitle: "Nope",
      enabledTitle: "Yep"
    }
  };
  render() {
    return <_DataEntry {...this.props} />;
  }
}

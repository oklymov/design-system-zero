import * as React from "react";
import { PropertyControls, ControlType } from "framer";
import { Heading as _Heading } from "../lib";

interface Props {
  text: string;
}

export class Heading extends React.Component<Props> {
  render() {
    return <_Heading {...this.props}>{this.props.text}</_Heading>;
  }

  static defaultProps: Props = {
    text: "Heading 1 40px"
  };

  static propertyControls: PropertyControls<Props> = {
    text: { type: ControlType.String, title: "Text" }
  };
}

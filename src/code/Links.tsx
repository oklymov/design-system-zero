import * as React from "react";
import { PropertyControls, ControlType } from "framer";
import { Links as _Links } from "../lib";

interface Props {
  text: string;
}

export class Links extends React.Component<Props> {
  render() {
    return <_Links {...this.props}>{this.props.text}</_Links>;
  }

  static defaultProps: Props = {
    text: "Link"
  };

  static propertyControls: PropertyControls<Props> = {
    text: { type: ControlType.String, title: "Text" }
  };
}

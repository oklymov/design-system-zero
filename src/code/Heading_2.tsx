import * as React from "react";
import { PropertyControls, ControlType } from "framer";
import { Heading2 as _Heading2 } from "../lib";

interface Props {
  text: string;
}

export class Heading2 extends React.Component<Props> {
  render() {
    return <_Heading2 {...this.props}>{this.props.text}</_Heading2>;
  }

  static defaultProps: Props = {
    text: "Heading 2 {size}"
  };

  static propertyControls: PropertyControls<Props> = {
    text: { type: ControlType.String, title: "Text" }
  };
}

import * as React from "react";
import { Code as _Code } from "../lib";
import { PropertyControls, ControlType } from "framer";

// Define type of property
interface Props {
  text: string;
}

export class Code extends React.Component<Props> {
  // Set default properties
  static defaultProps = {
    text: "Hello World!"
  };

  // Items shown in property panel
  static propertyControls: PropertyControls = {
    text: { type: ControlType.String, title: "Text" }
  };

  render() {
    return <_Code {...this.props}>{this.props.text}</_Code>;
  }
}
